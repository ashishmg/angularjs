var myApp =  angular.module('myApp', []);

// this is unminified version
/*  myApp.controller('mainController', function($scope, $http) {
        $http.get('lib/js/data.json').success(function(data) {
            $scope.person = data;
        });
    });
*/





    // this is minified version for minifing script
myApp.controller('mainController', ['$scope', '$http' , function ($scope, $http) {
	$http.get('lib/js/data.json').success(function(data){
    $scope.person = data;
    $scope.nameorder = 'name';
    });
	
}])

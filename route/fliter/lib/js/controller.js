var myController1 =  angular.module('myController1', []);


// this is unminified version
/*  myApp.controller('mainController', function($scope, $http) {
        $http.get('lib/js/data.json').success(function(data) {
            $scope.person = data;
        });
    });
*/

// this is minified version for minifing script

myController1.controller('listController', ['$scope', '$http' , function ($scope, $http) {
	$http.get('lib/js/data.json').success(function(data){
    $scope.person = data;
    $scope.nameorder = 'name';
    });
	
}]);

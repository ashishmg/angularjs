var myApp =  angular.module('myApp', [
'ngRoute',
'myController1'
]);

// this is unminified version
/*  myApp.controller('mainController', function($scope, $http) {
        $http.get('lib/js/data.json').success(function(data) {
            $scope.person = data;
        });
    });
*/





    // this is minified version for minifing script
myApp.config(['$routeProvider',function($routeProvider){
    $routeProvider.when('/List',{
        templateUrl: '../partials/list.html',
        controller: 'listController'
    }).
    otherwise({
        redirectTo: 'List'
    });
    
   // $routeProvider.when('/404',{
     //   templateUrl: '/route/404/404.html'
    //})
}]);